package com.example.moviesapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun isInternetAvailable(): Boolean {
    val result: Boolean
    val connectivityManager = SessionManager.context.getSystemService(
        Context.CONNECTIVITY_SERVICE
    ) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    return result
}

@SuppressLint("SimpleDateFormat")
fun convertTimeToAnotherFormat(
    currentPattern: String,
    targetPattern: String, senddate: String
): String {
    val originalFormat: DateFormat = SimpleDateFormat(currentPattern, Locale.ENGLISH)
    val targetFormat: DateFormat = SimpleDateFormat(targetPattern)
    return try {
        val date: Date? = originalFormat.parse(senddate)
        if (date != null) {
            targetFormat.format(date)
        } else {
            senddate
        }
    } catch (e: Exception) {
        e.printStackTrace()
        senddate
    }

}
