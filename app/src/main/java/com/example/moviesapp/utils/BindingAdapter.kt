package com.example.moviesapp.utils

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import timber.log.Timber

@SuppressLint("SetTextI18n")
@BindingAdapter("changeFormat")
fun changeFormat(view: TextView, s: String) {
    Timber.d("orderDetailDate $s")
    if(s!="" ) {
        val date =
            convertTimeToAnotherFormat("yyyy-MM-dd", "MMM dd, yyyy", s)
        view.text = "Date : $date"
    }
}

@BindingAdapter("imageFromUrl")
fun imageFromUrl(imgView: ImageView, imgUrl: String) {
    Glide.with(imgView.context)
        .load(imgUrl)
        .placeholder(R.drawable.nasa)
        .circleCrop()
        .into(imgView)

}

@BindingAdapter("rectangleImage")
fun rectangleImage(imgView: ImageView, imgUrl: String) {
    Glide.with(imgView.context)
        .load(imgUrl)
        .into(imgView)
}