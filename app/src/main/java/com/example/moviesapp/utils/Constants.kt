package com.example.moviesapp.utils

class Constants {
    companion object {
        //url
        const val BASE_URL = "https://api.nasa.gov/planetary/"

        //API Key
        const val API_KEY="Gfw1ioM8uyWWBA9bNJDpGHTiOQhumRfqTj16GTA3"

        //timeout
        const val SPLASH_TIME_OUT = 2000L
        const val CONNECT_TIMEOUT = 40
        const val TIMEOUT = 40000

        const val PAGE_SIZE = 10
        const val PAGE_INITIAL_LOAD_SIZE_HINT = 11
        const val PAGE_PREFETCH_DISTANCE = 10

    }
}