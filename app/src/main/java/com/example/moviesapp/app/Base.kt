package com.example.moviesapp.app

import android.app.Application
import androidx.room.Room
import com.example.moviesapp.room.AppDatabase
import com.example.moviesapp.room.HomeDao
import com.example.moviesapp.utils.SessionManager
import timber.log.Timber

class Base : Application() {
    override fun onCreate() {
        super.onCreate()
        SessionManager.initializeval(applicationContext)
        Timber.plant(Timber.DebugTree())

    }
    fun getDb():HomeDao{
        val db = Room.databaseBuilder(
            SessionManager.context,
            AppDatabase::class.java, "MovieDB26"
        ).allowMainThreadQueries()
            .build()
       return db.homeDao()
    }
}