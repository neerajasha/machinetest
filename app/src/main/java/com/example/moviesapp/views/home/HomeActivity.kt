package com.example.moviesapp.views.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.paging.map
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivityHomeBinding
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.views.details.DetailActivity
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var homeAdapter: HomeAdapter


    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupBinding()
        setupList()
        setUpObserver()
        setupView()
    }

    private fun setUpObserver() {
        viewModel.getList().observe(this, {
            if (homeAdapter.itemCount == 0 && !it.isNullOrEmpty())
                lifecycleScope.launch {
                    homeAdapter.submitData(PagingData.from(it))
                }
        })
    }

    @InternalCoroutinesApi
    fun setupView() {
        lifecycleScope.launch {
            viewModel.listData.collectLatest { pagingData ->
                Timber.d("homeAdapter ${homeAdapter.itemCount}//${viewModel.homeList.value}")
                homeAdapter.submitData(pagingData)
            }
        }
    }

    private fun setupList() {
        homeAdapter = HomeAdapter(HomeClickListener {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("detailData", it)
            startActivity(intent)
        })
        binding.homeRecycler.adapter = homeAdapter
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        viewModel =
            ViewModelProvider(
                this,
                HomeViewModelFactory(this)
            )[HomeViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

}