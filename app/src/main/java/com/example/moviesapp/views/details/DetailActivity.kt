package com.example.moviesapp.views.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivityDetailBinding
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.dataprovider.HomeResponse

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel
    var detailData: HomeData?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(intent!=null){
           detailData=intent.getParcelableExtra("detailData")
        }

        setupBinding()
        viewModel.setData(detailData)
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.getClicked().observe(this,{
            when(it){
                1->finish()
            }
        })
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }
}