package com.example.moviesapp.views.home

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.core.os.postDelayed
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.example.moviesapp.app.Base
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.dataprovider.HomeResponse
import com.example.moviesapp.network.ConnectionManager
import com.example.moviesapp.network.PostDataSource
import com.example.moviesapp.network.ResultCallBack
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel(private val appContext: Context) : ViewModel() {

    var homeList = MutableLiveData<ArrayList<HomeData>>()
    var showProgress = MutableLiveData<Boolean>()
    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        homeList.value = ArrayList()
        showProgress.value = false
        getMoviesApi()
    }

    fun getMoviesApi() {
        showProgress.value=true
        coroutineScope.launch {
            ConnectionManager.getDataManager().getMovies(
                object : ResultCallBack<ArrayList<HomeResponse>> {
                    override fun onError(code: Int, errorMessage: String) {
                        showProgress.value = false
                        Timber.d("homeResponse $errorMessage")
                    }

                    override fun <T> onSuccess(response: T) {
                        showProgress.value = false
                        Timber.d("homeResponse $response")
                        val temp: ArrayList<HomeData> = ArrayList()
                        if (response != null) {
                            val apiData = response as ArrayList<HomeResponse>
                            apiData.forEach { it1 ->
                                val homeData = HomeData(
                                    0,
                                    it1.copyright,
                                    it1.date,
                                    it1.explanation,
                                    it1.hdurl,
                                    it1.media_type,
                                    it1.service_version,
                                    it1.title,
                                    it1.url
                                )
                                temp.add(homeData)
                                val context = appContext.applicationContext as Base
                                context.getDb().insertAll(homeData)
                            }

                            Timber.d("homeData ${homeList.value}")
                            homeList.value = temp
                        }
                    }

                })
        }
    }

    fun getList(): LiveData<ArrayList<HomeData>> {
        return homeList
    }


    val listData = Pager(PagingConfig(pageSize = 1)) {
        PostDataSource(appContext)
    }.flow.cachedIn(viewModelScope)


}