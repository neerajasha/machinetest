package com.example.moviesapp.views.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.dataprovider.HomeResponse

class DetailViewModel : ViewModel() {
    var details= MutableLiveData<HomeData>()
    var clicked=MutableLiveData<Int>()

    init {}

    fun setData(detailData: HomeData?) {
        details.value = detailData
    }

    fun onClicked(value:Int){
        clicked.value=value
    }

    fun getClicked():LiveData<Int>{
        return clicked
    }
}