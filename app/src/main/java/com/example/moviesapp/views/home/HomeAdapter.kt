package com.example.moviesapp.views.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.databinding.HomeItemBinding
import com.example.moviesapp.dataprovider.HomeData
import timber.log.Timber

class HomeAdapter(private val clickListener:HomeClickListener) : PagingDataAdapter<HomeData, HomeAdapter.ViewHolder>(DataDifferntiator) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it,clickListener) }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: HomeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = HomeItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(
            item: HomeData,
            clickListener: HomeClickListener,

            ) {
            Timber.d("itemToBind $item")
            binding.viewModel=item
            binding.clickListener=clickListener

        }
    }


    object DataDifferntiator : DiffUtil.ItemCallback<HomeData>() {

        override fun areItemsTheSame(oldItem: HomeData, newItem: HomeData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: HomeData, newItem: HomeData): Boolean {
            return oldItem == newItem
        }
    }

}
class HomeClickListener(val clickListener: (data: HomeData) -> Unit) {
    fun onClick(data: HomeData) {
        clickListener(data)
    }
}