package com.example.moviesapp.network

import com.example.moviesapp.dataprovider.HomeResponse
import com.example.moviesapp.network.RetroApi.retrofitService
import com.example.moviesapp.utils.Constants
import com.example.moviesapp.utils.isInternetAvailable
import kotlinx.coroutines.withTimeout
import retrofit2.Response
import timber.log.Timber

class ConnectionManager {

    companion object {
        fun getDataManager(): ConnectionManager {
            return ConnectionManager()
        }
    }
    suspend fun getMovies(
        callBack: ResultCallBack<ArrayList<HomeResponse>>
    ) {
        val job = withTimeout(Constants.TIMEOUT.toLong()) {
            if (isInternetAvailable()) {
                val getPropertiesDeferred = retrofitService.getMoviesAsync(Constants.API_KEY,50)
                try {
                    val response = getPropertiesDeferred.await()
                    onResponseReceived(response, callBack)
                } catch (e: Exception) {
                    Timber.d("homeResponse exception-> ${e}")
                    e.message?.let { callBack.onError(1, "Something went wrong") }
                }
            } else
                callBack.onError(2, "Check your internet connection")
        }
        if (job == null)
            callBack.onError(2, "Connection timed out")
    }

    private fun <T> onResponseReceived(response: Response<T>, callBack: ResultCallBack<T>) {
        if (response.isSuccessful) {
            if (response.body() != null)
                callBack.onSuccess(response.body())
            else
                callBack.onError(1, response.message())
        } else {
            callBack.onError(1, response.message())

        }
    }
}
