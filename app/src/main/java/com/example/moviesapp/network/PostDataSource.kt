package com.example.moviesapp.network

import android.content.Context
import androidx.paging.*
import com.example.moviesapp.app.Base
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.utils.Constants
import timber.log.Timber
import kotlin.system.exitProcess


class PostDataSource(
    private val appContext: Context
) :
    PagingSource<Int, HomeData>() {
    val context = appContext.applicationContext as Base
    val homeDao = context.getDb()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, HomeData> {
        try {
            val key = params.key
            val currentLoadingPageKey: Int
            if (params.key == null)
                currentLoadingPageKey = 1
            else if (key != null && key < 50)
                currentLoadingPageKey = key
            else
                currentLoadingPageKey = 51
            Timber.d("loadeingresponse1 $currentLoadingPageKey")
            val response =
                if (currentLoadingPageKey < 50) homeDao.getAll(currentLoadingPageKey) else ArrayList()
            Timber.d("loadeingresponse $response")
            val responseData = mutableListOf<HomeData>()
            responseData.addAll(response)
            val prevKey = if (currentLoadingPageKey == 1) null else currentLoadingPageKey - 1
            if (currentLoadingPageKey <= 50)
                return LoadResult.Page(
                    data = responseData,
                    prevKey = prevKey,
                    nextKey =  currentLoadingPageKey.plus(1)
                )
            return LoadResult.Page(emptyList(), null, null)


        } catch (e: Exception) {
            Timber.d("loadeingresponseError ${e.message}")
            return LoadResult.Error(e)
        }
    }


    override fun getRefreshKey(state: PagingState<Int, HomeData>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

}
