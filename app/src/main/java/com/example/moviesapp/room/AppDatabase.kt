package com.example.moviesapp.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesapp.dataprovider.HomeData
import com.example.moviesapp.dataprovider.HomeResponse

@Database(entities = [HomeData::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun homeDao(): HomeDao
}