package com.example.moviesapp.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviesapp.dataprovider.HomeData

@Dao
interface HomeDao {
    @Query("SELECT * FROM HomeData LIMIT 10 OFFSET :offset")
    fun getAll(offset: Int): List<HomeData>

    @Insert
    fun insertAll(vararg home: HomeData)

}